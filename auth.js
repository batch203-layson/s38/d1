const jwt = require("jsonwebtoken");

/* 
Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret key
*/
const secret = "CourseBookingApi";

/* 
[SECTION] 

JSON Web Tokens

Token Creation
Analogy:
    Pack the gift provided with a lock, which can only be open using the secret codew as the key
*/

// the "user" parameter 
module.exports.createAccessToken= (user) =>{
    console.log(user);
    const data ={
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {});
}

/* 
Generate a JSON web token using the jwt's "sign method"

syntax
jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])
*/
