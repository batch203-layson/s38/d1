const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

/* 
Check if the email exists

Steps:
1. Use mongoose "find" method to find duplicate emails
2. Use "then" method to send a response back to the frontend application based on the result of the "find" method
*/

module.exports.checkEmailExists = (req, res) =>{
    return User.find({email: req.body.email}).then(result=>{

        /* 
            The result of the find() method returns an array of objects

            we can use array.length method for checking the current result length
        */
       console.log(result);

       /* 
       The user already exists
       */
       if(result.length > 0)
       {
            return res.send(true);
       }
        /* 
       There are no diplicate found user already exists
       */
       else
       {
        return res.send(false);
       }
    })
    .catch(error=> res.send(error));

    // User registration
    /*
        Steps:
        1. Create a new User object using the mongoose model and the information from the request body
        2. Make sure that the password is encrypted
        3. Save the new User to the database
    */

    
}


module.exports.registerUser = (req, res)=>{
    

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        /* 
        syntax:
        bcrypt.hashSync(dataToBeEncrypted, salt);
            salt - salt rounds that the bcrypt algorithm will run to encrypt the password
        */
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNumber: req.body.mobileNumber
    });

    console.log(newUser);

    return newUser.save()
    .then(user=>{
        console.log(user);
        // send true if the user is created
        res.send(true);
    })
    .catch(error=>{
        console.log(error);
        // send false if the user is created
        res.send(false);
    })
}

/* 
 User authentication

	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not


*/


module.exports.loginUser = (req, res) => {
    return User.findOne({ email: req.body.email })
        .then(result => {
            // User does not exists
            if (result == null) {
                //return res.send(false)
                return res.send({ message: "No User Found!" });
            }
            // User exists
            else {
                // Syntax: compareSync(data, encrypted)
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

                // If the passwords match/result of the above code is true.
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(result) });
                }
                else {
                    return res.send({ message: "Incorrect Password!" });
                }
            }
        })
}

// S38 Activity
/*
    1. Create a "/details/:id" route that will accept the user’s Id to retrieve the details of a user.
    2. Create a getProfile controller method for retrieving the details of the user:
        - Find the document in the database using the user's ID
        - Reassign the password of the returned document to an empty string ("") / ("*****")
        - Return the result back to the postman
    3. Process a GET request at the /details/:id route using postman to retrieve the details of the user.
    4. Updated your s37-41 remote link, push to git with the commit message of "Add activity code - S38".
    5. Add the link in Boodle.

*/


module.exports.userDetails = (req, res) => {


    let updates = {
        id: req.body.id
    }

    /* 
    syntax
    findByIdAndUpdate(_id, {objectUpdate}, options)
    {new:true} - returns the updated version of the document
    */
    User.findByIdAndUpdate(req.params.id, updates)
        .then(updatedTask => res.send(updatedTask))
        .catch(error => res.send(error));
}
