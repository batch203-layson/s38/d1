/* 

		course {

    id - unique for the document(auto generated)
			name,
        description,
        price,
        slots,
        isActive,
        createdOn,
        enrollees: [

            {
                id - document identifier(auto generated),
            userId,
            isPaid,
            dateEnrolled
				}
			]

		}

*/

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
    courseName: {
        type: String,
        required: [true, "First name is required"]
    },
    courseDescription: {
        type: String,
        required: [true, "Last name is required"]
    },
    coursePrice: {
        type: String,
        required: [true, "Email is required"]
    },
    courseSlots: {
        type: String,
        required: [true, "Password is required"]
    },
    courseIsActive: {
        type: Boolean,
        required: true
    },
    courseCreatedOn: {
        type: Date,
        default: new Date()
    },
    courseEnrollees: [
        {
            userId: {
                type: String,
                required: [true, "Course ID is required"]
            },
            isPaid: {
                type: Boolean,
                default: true
            },
            dateEnrolled: {
                type: Date,
                default: new Date()
            }
        }
    ]

});

// const Course = mongoose.model();

const Course = mongoose.model("course", courseSchema);
module.exports = Course;